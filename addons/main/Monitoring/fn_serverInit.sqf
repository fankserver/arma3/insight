#include "module.hpp"
/**
 * ArmA3Insight - Core - serverInit
 *
 * Author: Fank
 *
 * Description:
 * Initializes the collection loops on server-side
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */

["missionStarted", {
	[] call FUNC(register);
}] call CFUNC(addEventHandler);
