#include "module.hpp"
/**
 * ArmA3Insight - Monitoring - collect
 *
 * Author: Fank
 *
 * Description:
 * Collect insights on server-side and report to extension
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */

private _allObjects = allMissionObjects "all";

// objects
private _weaponHolderDeadLocal = 0;
private _weaponHolderDeadRemote = 0;
private _weaponHolderDroppedLocal = 0;
private _weaponHolderDroppedRemote = 0;
private _reammoBoxLocal = 0;
private _reammoBoxRemote = 0;
private _itemsBaseLocal = 0;
private _itemsBaseRemote = 0;
private _strategicLocal = 0;
private _strategicRemote = 0;
private _nonStrategicLocal = 0;
private _nonStrategicRemote = 0;

// vehicles
private _landAliveLocal = 0;
private _landAliveRemote = 0;
private _landDeadLocal = 0;
private _landDeadRemote = 0;
private _airAliveLocal = 0;
private _airAliveRemote = 0;
private _airDeadLocal = 0;
private _airDeadRemote = 0;
private _shipAliveLocal = 0;
private _shipAliveRemote = 0;
private _shipDeadLocal = 0;
private _shipDeadRemote = 0;

// unit
private _unitCAManBaseAliveLocal = 0;
private _unitCAManBaseAliveRemote = 0;
private _unitCAManBaseDeadLocal = 0;
private _unitCAManBaseDeadRemote = 0;
private _unitSoldierWB = 0;
private _unitSoldierEB = 0;
private _unitSoldierGB = 0;
private _unitCivilian = 0;

// simulation
private _simulationableObjects = 0;
private _simulatedObjects = 0;
private _dynamicSimulationSystemEnabled = 0;
private _dynamicSimulationEnabled = 0;
if (dynamicSimulationSystemEnabled) then {
	_dynamicSimulationSystemEnabled = 1;
};

{
	// objects
	if (_x isKindOf "GroundWeaponHolder") then {
		if (local _x) then {
			_weaponHolderDroppedLocal = _weaponHolderDroppedLocal + 1;
		} else {
			_weaponHolderDroppedRemote = _weaponHolderDroppedRemote + 1;
		};
	};
	if (typeOf _x == "WeaponHolderSimulated") then {
		if (local _x) then {
			_weaponHolderDeadLocal = _weaponHolderDeadLocal + 1;
		} else {
			_weaponHolderDeadRemote = _weaponHolderDeadRemote + 1;
		};
	};
	if (_x isKindOf "ReammoBox_F") then {
		if (local _x) then {
			_reammoBoxLocal = _reammoBoxLocal + 1;
		} else {
			_reammoBoxRemote = _reammoBoxRemote + 1;
		};
	};
	if (_x isKindOf "Items_base_F") then {
		if (local _x) then {
			_itemsBaseLocal = _itemsBaseLocal + 1;
		} else {
			_itemsBaseRemote = _itemsBaseRemote + 1;
		};
	};
	if (_x isKindOf "Strategic") then {
		if (local _x) then {
			_strategicLocal = _strategicLocal + 1;
		} else {
			_strategicRemote = _strategicRemote + 1;
		};
	};
	if (_x isKindOf "NonStrategic") then {
		if (local _x) then {
			_nonStrategicLocal = _nonStrategicLocal + 1;
		} else {
			_nonStrategicRemote = _nonStrategicRemote + 1;
		};
	};

	// vehicles
	if (_x isKindOf "LandVehicle") then {
		if (alive _x && local _x) then {
			_landAliveLocal = _landAliveLocal + 1;
		};
		if (alive _x && !local _x) then {
			_landAliveRemote = _landAliveRemote + 1;
		};
		if (!alive _x && local _x) then {
			_landDeadLocal = _landDeadLocal + 1;
		};
		if (!alive _x && !local _x) then {
			_landDeadRemote = _landDeadRemote + 1;
		};
	};
	if (_x isKindOf "Air") then {
		if (alive _x && local _x) then {
			_airAliveLocal = _airAliveLocal + 1;
		};
		if (alive _x && !local _x) then {
			_airAliveRemote = _airAliveRemote + 1;
		};
		if (!alive _x && local _x) then {
			_airDeadLocal = _airDeadLocal + 1;
		};
		if (!alive _x && !local _x) then {
			_airDeadRemote = _airDeadRemote + 1;
		};
	};
	if (_x isKindOf "Ship") then {
		if (alive _x && local _x) then {
			_shipAliveLocal = _shipAliveLocal + 1;
		};
		if (alive _x && !local _x) then {
			_shipAliveRemote = _shipAliveRemote + 1;
		};
		if (!alive _x && local _x) then {
			_shipDeadLocal = _shipDeadLocal + 1;
		};
		if (!alive _x && !local _x) then {
			_shipDeadRemote = _shipDeadRemote + 1;
		};
	};

	// units
	if (_x isKindOf "CAManBase") then {
		if (alive _x && local _x) then {
			_unitCAManBaseAliveLocal = _unitCAManBaseAliveLocal + 1;
		};
		if (alive _x && !local _x) then {
			_unitCAManBaseAliveRemote = _unitCAManBaseAliveRemote + 1;
		};
		if (!alive _x && local _x) then {
			_unitCAManBaseDeadLocal = _unitCAManBaseDeadLocal + 1;
		};
		if (!alive _x && !local _x) then {
			_unitCAManBaseDeadRemote = _unitCAManBaseDeadRemote + 1;
		};
	};
	if (_x isKindOf "SoldierWB") then {
		_unitSoldierWB = _unitSoldierWB + 1;
	};
	if (_x isKindOf "SoldierEB") then {
		_unitSoldierEB = _unitSoldierEB + 1;
	};
	if (_x isKindOf "SoldierGB") then {
		_unitSoldierGB = _unitSoldierGB + 1;
	};
	if (_x isKindOf "Civilian") then {
		_unitCivilian = _unitCivilian + 1;
	};

	// simulation
	if (local _x) then {
		_simulationableObjects = _simulationableObjects + 1;
		if (simulationEnabled _x) then {
			_simulatedObjects = _simulatedObjects + 1;
		};
	};
	if (dynamicSimulationEnabled _x) then {
		_dynamicSimulationEnabled = _dynamicSimulationEnabled + 1;
	};
} forEach _allObjects;

EXTENSION callExtension ["object_count", [
	count _allObjects,
	_weaponHolderDeadLocal,
	_weaponHolderDeadRemote,
	_weaponHolderDroppedLocal,
	_weaponHolderDroppedRemote,
	_reammoBoxLocal,
	_reammoBoxRemote,
	_itemsBaseLocal,
	_itemsBaseRemote,
	_strategicLocal,
	_strategicRemote,
	_nonStrategicLocal,
	_nonStrategicRemote
]];
EXTENSION callExtension ["vehicle_count", [
	_landAliveLocal,
	_landAliveRemote,
	_landDeadLocal,
	_landDeadRemote,
	_airAliveLocal,
	_airAliveRemote,
	_airDeadLocal,
	_airDeadRemote,
	_shipAliveLocal,
	_shipAliveRemote,
	_shipDeadLocal,
	_shipDeadRemote
]];
EXTENSION callExtension ["unit_count", [
	_unitCAManBaseAliveLocal,
	_unitCAManBaseAliveRemote,
	_unitCAManBaseDeadLocal,
	_unitCAManBaseDeadRemote,
	_unitSoldierWB,
	_unitSoldierEB,
	_unitSoldierGB,
	_unitCivilian
]];

// players
private _playerTotal = 0;
private _playerHC = 0;
private _playerEast = 0;
private _playerWest = 0;
private _playerResistance = 0;
private _playerCivilian = 0;

private _headlessClients = entities "HeadlessClient_F";
_playerHC = count _headlessClients;
private _allPlayers = allPlayers;
_playerTotal = count _allPlayers;
private _humanPlayers = _allPlayers - _headlessClients;
_playerEast = east countSide _humanPlayers;
_playerWest = west countSide _humanPlayers;
_playerResistance = resistance countSide _humanPlayers;
_playerCivilian = civilian countSide _humanPlayers;

EXTENSION callExtension ["player_count", [
	_playerTotal,
	_playerHC,
	_playerEast,
	_playerWest,
	_playerResistance,
	_playerCivilian
]];

// performance
private _diagTickTime = diag_tickTime;
private _diagFPS = diag_fps;
private _diagFPSMin = diag_fpsMin;
private _diagActiveScripts = diag_activeScripts;

// performance (CBA)
private _cbaWaitAndExecCount = 0;
isNil {
	_cbaWaitAndExecCount = count cba_common_waitAndExecArray;
};
private _cbaWaitUntilAndExecCount = 0;
isNil {
	_cbaWaitUntilAndExecCount = count cba_common_waitUntilAndExecArray;
};
private _cbaFrameCount = 0;
isNil {
	_cbaFrameCount = count cba_common_perFrameHandlerArray;
};
private _cbaFrameMax = 0;
isNil {
	_cbaFrameMax = count cba_common_PFHhandles;
};

// performance (CLib)
private _clibFrameCount = 0;
isNil {
	_clibFrameCount = count CLib_Perframe_perFrameHandlerArray;
};

EXTENSION callExtension ["performance", [
	_diagTickTime,
	_diagFPS,
	_diagFPSMin,
	_diagActiveScripts select 0,
	_diagActiveScripts select 1,
	_diagActiveScripts select 2,
	_diagActiveScripts select 3,
	_cbaWaitAndExecCount,
	_cbaWaitUntilAndExecCount,
	_cbaFrameCount,
	_cbaFrameMax,
	_clibFrameCount
]];

EXTENSION callExtension ["simulation", [
	_simulationableObjects,
	_simulatedObjects,
	_dynamicSimulationSystemEnabled,
	_dynamicSimulationEnabled
]];

// Client FPS
if (count EGVAR(Client,Queue) > 0) then {
	private _clientQueue = EGVAR(Client,Queue);
	EGVAR(Client,Queue) = [];

	EXTENSION callExtension ["client_fps", [_diagTickTime, str _clientQueue]];
};
