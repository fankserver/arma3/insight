#include "module.hpp"
/**
 * ArmA3Insight - Monitoring - register
 *
 * Author: Fank
 *
 * Description:
 * Initializes the extension and register the loop for insights
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */
EXTENSION callExtension ["init", [serverName, missionName]];

["itemAdd", [
	BI_LOOP,
	{
		[] call FUNC(collect);
	},
	5
]] call BIS_fnc_loop;
