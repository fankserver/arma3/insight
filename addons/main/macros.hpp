#include "script_version.hpp"

#define PREFIX Insight
#define PATH fs
#define MOD main

#define EXTENSION QUOTE(insight)

// CLib override
#define PATCHLVL PATCH
#include "\tc\CLib\addons\CLib\macros.hpp"
