#include "\tc\CLib\addons\CLib\ModuleMacros.hpp"

class CfgCLibModules {
	class PREFIX {
		path = "\fs\insight\addons\main";

		MODULE(Client) {
			dependency[] = {"CLib/Events"};
			FNC(clientInit);
			FNC(collect);
			FNC(register);
			FNC(serverInit);
			FNC(unregister);
		};

		MODULE(Monitoring) {
			dependency[] = {"CLib/Events"};
			FNC(collect);
			FNC(register);
			FNC(serverInit);
			FNC(unregister);
		};
	};
};
