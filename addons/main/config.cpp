#include "macros.hpp"
class CfgPatches {
	class PREFIX {
		units[] = {};
		weapons[] = {};
		requiredVersion = 2.00;
		author = "Fankserver";
		authors[] = {"Fank"};
		authorUrl = "";
		version = VERSION;
		versionStr = QUOTE(VERSION);
		versionAr[] = {VERSION_AR};
		requiredAddons[] = {"CLib"};
	};
};

#include "modules.hpp"
