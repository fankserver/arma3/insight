#include "module.hpp"
/**
 * ArmA3Insight - Core - serverInit
 *
 * Author: Fank
 *
 * Description:
 * Initializes the queue logic on server-side
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */


GVAR(Queue) = [];

[CLIENT_FPS_RECEIVE, {
	_this select 0 params["_playerName", "_playerUID", "_fps"];
	GVAR(Queue) append [[
		diag_tickTime,
		_playerName,
		_playerUID,
		_fps
	]];
}] call CFUNC(addEventHandler);
