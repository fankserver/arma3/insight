#include "module.hpp"
/**
 * ArmA3Insight - Client - register
 *
 * Author: Fank
 *
 * Description:
 * Register the loop for insight
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */

GVAR(FrameNumber) = diag_frameNo;
GVAR(FrameTickTime) = diag_tickTime;

["itemAdd", [
	BI_LOOP,
	{
		[] call FUNC(collect);
	},
	CLIENT_REPORT_DELAY
]] call BIS_fnc_loop;
