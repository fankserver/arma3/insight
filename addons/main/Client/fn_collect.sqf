#include "module.hpp"
/**
 * ArmA3Insight - Client - collect
 *
 * Author: Fank
 *
 * Description:
 * Collect insights on client-side and send to server
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */

private _lastFrameNo = diag_frameNo;
private _lastTick = diag_tickTime;
private _numberOfFrames = _lastFrameNo - GVAR(FrameNumber);
private _passedTime = _lastTick - GVAR(FrameTickTime);
GVAR(FrameNumber) = _lastFrameNo;
GVAR(FrameTickTime) = _lastTick;

[CLIENT_FPS_RECEIVE, [
	name player,
	getPlayerUID player,
	_numberOfFrames / _passedTime
]] call CFUNC(serverEvent);
