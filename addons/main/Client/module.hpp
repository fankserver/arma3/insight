#define MODULE Client
#include "\fs\insight\addons\main\macros.hpp"

#define BI_LOOP "InsightClientCollector"
#define CLIENT_FPS_RECEIVE "clientFPSInfoReceived"
#define CLIENT_REPORT_INITIAL_DELAY 5
#define CLIENT_REPORT_DELAY 10
