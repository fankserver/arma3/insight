#include "module.hpp"
/**
 * ArmA3Insight - Client - clientInit
 *
 * Author: Fank
 *
 * Description:
 * Initializes the insights on the client-side
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */

["missionStarted", {
	// delay according to sender group -> Space out client-server communication as evenly as possible
	private _delay = diag_tickTime random CLIENT_REPORT_INITIAL_DELAY;

	[
		{
			[] call FUNC(register);
		},
		_delay,
		[]
	] call CFUNC(wait);
}] call CFUNC(addEventHandler);
