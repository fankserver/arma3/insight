#include "module.hpp"
/**
 * ArmA3Insight - Client - unregister
 *
 * Author: Fank
 *
 * Description:
 * Unregister the loop for insights
 *
 * Parameter(s):
 * 0: None <Any>
 *
 * Return Value:
 * None <Any>
 *
 */

["itemRemove", [BI_LOOP]] call BIS_fnc_loop;
