# Insight

Build windows
```bash
go build -o insight_x64.dll -buildmode=c-shared ./src/extension
```

Build linux
```bash
go build -o insight_x64.so -buildmode=c-shared ./src/extension
```
