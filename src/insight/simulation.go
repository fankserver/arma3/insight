package insight

import (
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/sirupsen/logrus"
)

type Simulation struct {
	SimulationTotal                int
	SimulationEnabled              int
	DynamicSimulationSystemEnabled int
	DynamicSimulationEnabled       int
}

func (i *Insight) LogSimulation(si Simulation) {
	logrus.Debugln("simulation")

	if i.influxWriteAPI != nil {
		i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
			"simulation",
			i.getTags(),
			map[string]interface{}{
				"simulation_total":                  si.SimulationTotal,
				"simulation_enabled":                si.SimulationEnabled,
				"dynamic_simulation_system_enabled": si.DynamicSimulationSystemEnabled,
				"dynamic_simulation_enabled":        si.DynamicSimulationEnabled,
			},
			time.Now(),
		))
	}
}
