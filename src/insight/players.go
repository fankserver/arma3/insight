package insight

import (
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/sirupsen/logrus"
)

type PlayerCount struct {
	Total          int
	HeadlessClient int
	East           int
	West           int
	Resistance     int
	Civilian       int
}

func (i *Insight) LogPlayerCount(pc PlayerCount) {
	logrus.Debugln("player count")

	if i.influxWriteAPI != nil {
		i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
			"players",
			i.getTags(),
			map[string]interface{}{
				"total":           pc.Total,
				"headless_client": pc.HeadlessClient,
				"east":            pc.East,
				"west":            pc.West,
				"resistance":      pc.Resistance,
				"civilian":        pc.Civilian,
			},
			time.Now(),
		))
	}
}
