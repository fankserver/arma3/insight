package insight

import (
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/sirupsen/logrus"
)

type Performance struct {
	TickTime               float64
	FPS                    float64
	FPSMin                 float64
	ActiveScriptSpawn      int
	ActiveScriptExecVM     int
	ActiveScriptExec       int
	ActiveScriptExecFSM    int
	CBAWaitAndExec         int
	CBAWaitUntilAndExec    int
	CBAFrameHandler        int
	CBAFrameHandlerHandles int
	CLibFrameHandler       int
}

func (i *Insight) LogPerformance(p Performance) {
	logrus.Debugln("performance")

	if i.influxWriteAPI != nil {
		i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
			"performance",
			i.getTags(),
			map[string]interface{}{
				"ticktime":              p.TickTime,
				"fps":                   p.FPS,
				"fps_min":               p.FPSMin,
				"active_script_spawn":   p.ActiveScriptSpawn,
				"active_script_execvm":  p.ActiveScriptExecVM,
				"active_script_exec":    p.ActiveScriptExec,
				"active_script_execfsm": p.ActiveScriptExecFSM,
				"cba_waitandexec":       p.CBAWaitAndExec,
				"cba_waituntilandexec":  p.CBAWaitUntilAndExec,
				"cba_fh":                p.CBAFrameHandler,
				"cba_fh_handles":        p.CBAFrameHandlerHandles,
				"clib_fh":               p.CLibFrameHandler,
			},
			time.Now(),
		))
	}

	if i.fpsLog != nil {
		i.fpsLog <- logFPS{
			Name: "__Server",
			GUID: "0",
			Time: time.Now(),
			FPS:  p.FPS,
		}
	}
}
