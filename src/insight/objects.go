package insight

import (
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/sirupsen/logrus"
)

type ObjectCount struct {
	Total                     int
	WeaponHolderDeadLocal     int
	WeaponHolderDeadRemote    int
	WeaponHolderDroppedLocal  int
	WeaponHolderDroppedRemote int
	ReammoBoxLocal            int
	ReammoBoxRemote           int
	ItemsBaseLocal            int
	ItemsBaseRemote           int
	StrategicLocal            int
	StrategicRemote           int
	NonStrategicLocal         int
	NonStrategicRemote        int
}

func (i *Insight) LogObjectCount(objectCount ObjectCount) {
	logrus.Debugln("object count")

	if i.influxWriteAPI != nil {
		i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
			"objects",
			i.getTags(),
			map[string]interface{}{
				"total":                       objectCount.Total,
				"weaponholder":                objectCount.WeaponHolderDeadLocal + objectCount.WeaponHolderDeadRemote + objectCount.WeaponHolderDroppedLocal + objectCount.WeaponHolderDroppedRemote,
				"weaponholder_dead":           objectCount.WeaponHolderDeadLocal + objectCount.WeaponHolderDeadRemote,
				"weaponholder_dead_local":     objectCount.WeaponHolderDeadLocal,
				"weaponholder_dead_remote":    objectCount.WeaponHolderDeadRemote,
				"weaponholder_dropped":        objectCount.WeaponHolderDroppedLocal + objectCount.WeaponHolderDroppedRemote,
				"weaponholder_dropped_local":  objectCount.WeaponHolderDroppedLocal,
				"weaponholder_dropped_remote": objectCount.WeaponHolderDroppedRemote,
				"reammobox":                   objectCount.ReammoBoxLocal + objectCount.ReammoBoxRemote,
				"reammobox_local":             objectCount.ReammoBoxLocal,
				"reammobox_remote":            objectCount.ReammoBoxRemote,
				"items_base":                  objectCount.ItemsBaseLocal + objectCount.ItemsBaseRemote,
				"items_base_local":            objectCount.ItemsBaseLocal,
				"items_base_remote":           objectCount.ItemsBaseRemote,
				"strategic":                   objectCount.StrategicLocal + objectCount.StrategicRemote,
				"strategic_local":             objectCount.StrategicLocal,
				"strategic_remote":            objectCount.StrategicRemote,
				"nonstrategic":                objectCount.NonStrategicLocal + objectCount.NonStrategicRemote,
				"nonstrategic_local":          objectCount.NonStrategicLocal,
				"nonstrategic_remote":         objectCount.NonStrategicRemote,
			},
			time.Now(),
		))
	}
}
