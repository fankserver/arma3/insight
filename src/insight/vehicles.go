package insight

import (
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/sirupsen/logrus"
)

type VehicleCount struct {
	LandAliveLocal  int
	LandAliveRemote int
	LandDeadLocal   int
	LandDeadRemote  int
	AirAliveLocal   int
	AirAliveRemote  int
	AirDeadLocal    int
	AirDeadRemote   int
	ShipAliveLocal  int
	ShipAliveRemote int
	ShipDeadLocal   int
	ShipDeadRemote  int
}

func (i *Insight) LogVehicleCount(vc VehicleCount) {
	logrus.Debugln("vehicle count")

	if i.influxWriteAPI != nil {
		i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
			"vehicles",
			i.getTags(),
			map[string]interface{}{
				"total":             vc.LandAliveLocal + vc.LandAliveRemote + vc.LandDeadLocal + vc.LandDeadRemote + vc.AirAliveLocal + vc.AirAliveRemote + vc.AirDeadLocal + vc.AirDeadRemote + vc.ShipAliveLocal + vc.ShipAliveRemote + vc.ShipDeadLocal + vc.ShipDeadRemote,
				"alive":             vc.LandAliveLocal + vc.LandAliveRemote + vc.AirAliveLocal + vc.AirAliveRemote + vc.ShipAliveLocal + vc.ShipAliveRemote,
				"alive_local":       vc.LandAliveLocal + vc.AirAliveLocal + vc.ShipAliveLocal,
				"alive_remote":      vc.LandAliveRemote + vc.AirAliveRemote + vc.ShipAliveRemote,
				"dead":              vc.LandDeadLocal + vc.LandDeadRemote + vc.AirDeadLocal + vc.AirDeadRemote + vc.ShipDeadLocal + vc.ShipDeadRemote,
				"dead_local":        vc.LandDeadLocal + vc.AirDeadLocal + vc.ShipDeadLocal,
				"dead_remote":       vc.LandDeadRemote + vc.AirDeadRemote + vc.ShipDeadRemote,
				"land":              vc.LandAliveLocal + vc.LandAliveRemote + vc.LandDeadLocal + vc.LandDeadRemote,
				"land_alive":        vc.LandAliveLocal + vc.LandAliveRemote,
				"land_alive_local":  vc.LandAliveLocal,
				"land_alive_remote": vc.LandAliveRemote,
				"land_dead":         vc.LandDeadLocal + vc.LandDeadRemote,
				"land_dead_local":   vc.LandDeadLocal,
				"land_dead_remote":  vc.LandDeadRemote,
				"air":               vc.AirAliveLocal + vc.AirAliveRemote + vc.AirDeadLocal + vc.AirDeadRemote,
				"air_alive":         vc.AirAliveLocal + vc.AirAliveRemote,
				"air_alive_local":   vc.AirAliveLocal,
				"air_alive_remote":  vc.AirAliveRemote,
				"air_dead":          vc.AirDeadLocal + vc.AirDeadRemote,
				"air_dead_local":    vc.AirDeadLocal,
				"air_dead_remote":   vc.AirDeadRemote,
				"ship":              vc.ShipAliveLocal + vc.ShipAliveRemote + vc.ShipDeadLocal + vc.ShipDeadRemote,
				"ship_alive":        vc.ShipAliveLocal + vc.ShipAliveRemote,
				"ship_alive_local":  vc.ShipAliveLocal,
				"ship_alive_remote": vc.ShipAliveRemote,
				"ship_dead":         vc.ShipDeadLocal + vc.ShipDeadRemote,
				"ship_dead_local":   vc.ShipDeadLocal,
				"ship_dead_remote":  vc.ShipDeadRemote,
			},
			time.Now(),
		))
	}
}
