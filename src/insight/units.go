package insight

import (
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/sirupsen/logrus"
)

type UnitCount struct {
	AliveLocal  int
	AliveRemote int
	DeadLocal   int
	DeadRemote  int
	SoldierWB   int
	SoldierEB   int
	SoldierGB   int
	Civilian    int
}

func (i *Insight) LogUnitCount(uc UnitCount) {
	logrus.Debugln("unit count")

	if i.influxWriteAPI != nil {
		i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
			"units",
			i.getTags(),
			map[string]interface{}{
				"total":        uc.SoldierWB + uc.SoldierEB + uc.SoldierGB + uc.Civilian,
				"alive":        uc.AliveLocal + uc.AliveRemote,
				"alive_local":  uc.AliveLocal,
				"alive_remote": uc.AliveRemote,
				"dead":         uc.DeadLocal + uc.DeadRemote,
				"dead_local":   uc.DeadLocal,
				"dead_remote":  uc.DeadRemote,
				"soldier_wb":   uc.SoldierWB,
				"soldier_eb":   uc.SoldierEB,
				"soldier_gb":   uc.SoldierGB,
				"civilian":     uc.Civilian,
			},
			time.Now(),
		))
	}
}
