package insight

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/fankserver/arma3/insight/src/config"
)

type Insight struct {
	ServerName  string
	MissionName string

	Callback chan Callback
	fpsLog   chan logFPS

	callsLock sync.Mutex
	calls     map[string]int64

	influxClient   influxdb2.Client
	influxWriteAPI api.WriteAPI
}

type Callback struct {
	Function string
	Data     string
}

func NewInsight() (*Insight, error) {
	viper.SetConfigName("insight")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("$HOME/.config")
	for _, v := range os.Args {
		args := strings.SplitN(v, "=", 2)
		if len(args) != 2 || args[0] != "-insightConfig" {
			continue
		}
		viper.SetConfigFile(args[1])
		break
	}
	err := viper.ReadInConfig()
	if err != nil {
		logrus.Errorln(err.Error())
		return nil, err
	}
	err = viper.Unmarshal(&config.C)
	if err != nil {
		logrus.Errorln(err.Error())
		return nil, err
	}

	var file *os.File
	if config.C.Logging.Path != "" {
		file, err = os.Create(path.Join(config.C.Logging.Path, time.Now().Format(config.C.Logging.File)))
		if err != nil {
			return nil, err
		}
	}
	if file == nil {
		file, err = ioutil.TempFile("", "arma3_insight_*.log")
		if err != nil {
			return nil, err
		}
	}
	logrus.SetOutput(file)

	switch config.C.Logging.Level {
	case "trace":
		logrus.SetLevel(logrus.TraceLevel)
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	}
	logrus.SetFormatter(&logrus.JSONFormatter{})

	i := Insight{
		Callback: make(chan Callback),
		calls:    make(map[string]int64),
	}
	if config.C.InfluxDB.Enabled {
		i.influxClient = influxdb2.NewClient(config.C.InfluxDB.URL, config.C.InfluxDB.Token)
		i.influxWriteAPI = i.influxClient.WriteAPI(config.C.InfluxDB.Org, config.C.InfluxDB.Bucket)

		errorsCh := i.influxWriteAPI.Errors()
		go func() {
			for err := range errorsCh {
				logrus.Errorln("influx write error", err.Error())
			}
		}()
		go func() {
			ticker := time.NewTicker(1 * time.Second)
			for range ticker.C {
				t := time.Now()
				i.callsLock.Lock()
				for k, v := range i.calls {
					tags := i.getTags()
					tags["function"] = k
					i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
						"extension_call",
						tags,
						map[string]interface{}{
							"total": v,
						},
						t,
					))
				}
				i.callsLock.Unlock()
			}
		}()
	}

	if config.C.FPSFile.Enabled && config.C.FPSFile.Path != "" {
		logrus.Debugln("enable fps logging to file")
		i.fpsLog = make(chan logFPS, 20)
		go i.logFPSToFile()
	}

	return &i, nil
}

func (i *Insight) CallIncrease(function string) {
	i.callsLock.Lock()
	defer i.callsLock.Unlock()
	call, ok := i.calls[function]
	if !ok {
		call = 0
	}
	call++
	i.calls[function] = call
}

func (i *Insight) getTags() map[string]string {
	tags := map[string]string{}

	if config.C.Environment != "" {
		tags["environment"] = config.C.Environment
	}
	if i.ServerName != "" {
		tags["server"] = i.ServerName
	}
	if i.MissionName != "" {
		tags["mission"] = i.MissionName
	}

	return tags
}

func (i *Insight) logFPSToFile() {
	clientFPSFile, err := os.Create(path.Join(config.C.FPSFile.Path, time.Now().Format(config.C.FPSFile.File)))
	if err != nil {
		logrus.Errorln(err.Error())
		return
	}
	defer clientFPSFile.Close()

	for v := range i.fpsLog {
		_, err = clientFPSFile.WriteString(fmt.Sprintf(
			"%s	%s	%s	%.3f\n",
			v.Time.Format("2006-01-02 15:04:05"),
			v.Name,
			v.GUID,
			v.FPS,
		))
		if err != nil {
			logrus.Errorln(err.Error())
		}
	}
}

type logFPS struct {
	Time time.Time
	Name string
	GUID string
	FPS  float64
}
