package insight

import (
	"time"

	"gitlab.com/fankserver/arma3/insight/src/sqf"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"

	"github.com/sirupsen/logrus"
)

type FPS struct {
	Tick float64 `sqf:"0"`
	Name string  `sqf:"1"`
	GUID string  `sqf:"2"`
	FPS  float64 `sqf:"3"`
}

func (i *Insight) ClientFPS(tickTime float64, values string) {
	logrus.Debugln("client fps")
	logrus.Traceln(values)

	var fps []FPS
	err := sqf.UnquotedUnmarshal([]byte(values), &fps)
	if err != nil {
		logrus.Errorln(err.Error())
		return
	}

	t := time.Now()
	for _, v := range fps {
		logTime := t.Add(time.Duration(tickTime-v.Tick) * time.Second)

		if i.influxWriteAPI != nil {
			tags := i.getTags()
			tags["client_name"] = v.Name
			tags["client_id"] = v.GUID

			i.influxWriteAPI.WritePoint(influxdb2.NewPoint(
				"client_fps",
				tags,
				map[string]interface{}{
					"fps": v.FPS,
				},
				logTime,
			))
		}

		if i.fpsLog != nil {
			i.fpsLog <- logFPS{
				Name: v.Name,
				GUID: v.GUID,
				Time: logTime,
				FPS:  v.FPS,
			}
		}
	}
}
