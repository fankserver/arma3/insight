package config

var C Config

type Config struct {
	Environment string   `json:"environment" yaml:"environment" toml:"environment"`
	Logging     Logging  `json:"logging" yaml:"logging" toml:"logging"`
	InfluxDB    InfluxDB `json:"influxdb" yaml:"influxdb" toml:"influxdb"`
	FPSFile     FPSFile  `json:"fpsfile" yaml:"fpsfile" toml:"fpsfile"`
}

type Logging struct {
	Level string `json:"level" yaml:"level" toml:"level"`
	Path  string `json:"path" yaml:"path" toml:"path"`
	File  string `json:"file" yaml:"file" toml:"file"`
}

type InfluxDB struct {
	Enabled bool   `json:"enabled" yaml:"enabled" toml:"enabled"`
	URL     string `json:"url" yaml:"url" toml:"url"`
	Token   string `json:"token" yaml:"token" toml:"token"`
	Bucket  string `json:"bucket" yaml:"bucket" toml:"bucket"`
	Org     string `json:"org" yaml:"org" toml:"org"`
}

type FPSFile struct {
	Enabled bool   `json:"enabled" yaml:"enabled" toml:"enabled"`
	Path    string `json:"path" yaml:"path" toml:"path"`
	File    string `json:"file" yaml:"file" toml:"file"`
}
