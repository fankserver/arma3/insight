#include <stdlib.h>

#include "extensionCallback.h"

extern void goRVExtension(char *output, size_t outputSize, char *input);
extern void goRVExtensionVersion(char *output, size_t outputSize);
extern void goRVExtensionArgs(char* output, size_t outputSize, char* input, char** argv, int argc);
extern void goRVExtensionRegisterCallback(extensionCallback fnc);

#ifdef _WIN32
    #define EXPORT_API __declspec (dllexport)
#else
    #define EXPORT_API __attribute__((visibility("default")))
#endif

#if defined(WIN64) || defined(__x86_64__)
EXPORT_API void RVExtension(char *output, size_t outputSize, char *input) {
	goRVExtension(output, outputSize, input);
}

EXPORT_API void RVExtensionVersion(char *output, size_t outputSize) {
	goRVExtensionVersion(output, outputSize);
}

EXPORT_API void RVExtensionArgs(char* output, size_t outputSize, char* input, char** argv, int argc) {
	goRVExtensionArgs(output, outputSize, input, argv, argc);
}

EXPORT_API void RVExtensionRegisterCallback(extensionCallback fnc) {
	goRVExtensionRegisterCallback(fnc);
}
#else
EXPORT_API void __stdcall _RVExtension(char *output, size_t outputSize, char *input) {
	goRVExtension(output, outputSize, input);
}

EXPORT_API void __stdcall _RVExtensionVersion(char *output, size_t outputSize) {
	goRVExtensionVersion(output, outputSize);
}

EXPORT_API void __stdcall _RVExtensionArgs(char* output, size_t outputSize, char* input, char** argv, int argc) {
	goRVExtensionArgs(output, outputSize, input, argv, argc);
}

EXPORT_API void __stdcall _RVExtensionRegisterCallback(extensionCallback fnc) {
	goRVExtensionRegisterCallback(fnc);
}
#endif
