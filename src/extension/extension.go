package main

/*
   #include <stdlib.h>
   #include <stdio.h>
   #include <string.h>
   #include "extensionCallback.h"
*/
import "C"

import (
	"strconv"
	"sync"
	"unsafe"

	"github.com/sirupsen/logrus"

	armaInsight "gitlab.com/fankserver/arma3/insight/src/insight"
)

var (
	extensionCallbackFnc C.extensionCallback
	insight              *armaInsight.Insight
	insightLock          sync.Mutex
)

func runExtensionCallback(function string, data string) C.int {
	nameC := C.CString("insight")
	defer C.free(unsafe.Pointer(nameC))
	functionC := C.CString(function)
	defer C.free(unsafe.Pointer(functionC))
	dataC := C.CString(data)
	defer C.free(unsafe.Pointer(dataC))
	return C.runExtensionCallback(extensionCallbackFnc, nameC, functionC, dataC)
}

//export goRVExtensionVersion
func goRVExtensionVersion(output *C.char, outputsize C.size_t) {
	result := C.CString("Version 1.0")
	defer C.free(unsafe.Pointer(result))
	var size = C.strlen(result) + 1
	if size > outputsize {
		size = outputsize
	}
	C.memmove(unsafe.Pointer(output), unsafe.Pointer(result), size)
}

//export goRVExtensionArgs
func goRVExtensionArgs(output *C.char, outputsize C.size_t, input *C.char, argv **C.char, argc C.int) {
	checkInit()
	function := C.GoString(input)
	go insight.CallIncrease(function)

	switch function {
	case "client_fps":
		tickTime := 0.0
		values := ""

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				tickTime, _ = strconv.ParseFloat(argValue, 64)
			case 1:
				values = argValue
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.ClientFPS(tickTime, values)
	case "init":
		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				v, _ := strconv.Unquote(argValue)
				insight.ServerName = v
			case 1:
				v, _ := strconv.Unquote(argValue)
				insight.MissionName = v
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}
	case "object_count":
		var objectCount armaInsight.ObjectCount

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				objectCount.Total, _ = strconv.Atoi(argValue)
			case 1:
				objectCount.WeaponHolderDeadLocal, _ = strconv.Atoi(argValue)
			case 2:
				objectCount.WeaponHolderDeadRemote, _ = strconv.Atoi(argValue)
			case 3:
				objectCount.WeaponHolderDroppedLocal, _ = strconv.Atoi(argValue)
			case 4:
				objectCount.WeaponHolderDroppedRemote, _ = strconv.Atoi(argValue)
			case 5:
				objectCount.ReammoBoxLocal, _ = strconv.Atoi(argValue)
			case 6:
				objectCount.ReammoBoxRemote, _ = strconv.Atoi(argValue)
			case 7:
				objectCount.ItemsBaseLocal, _ = strconv.Atoi(argValue)
			case 8:
				objectCount.ItemsBaseRemote, _ = strconv.Atoi(argValue)
			case 9:
				objectCount.StrategicLocal, _ = strconv.Atoi(argValue)
			case 10:
				objectCount.StrategicRemote, _ = strconv.Atoi(argValue)
			case 11:
				objectCount.NonStrategicLocal, _ = strconv.Atoi(argValue)
			case 12:
				objectCount.NonStrategicRemote, _ = strconv.Atoi(argValue)
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.LogObjectCount(objectCount)
	case "vehicle_count":
		var vehicleCount armaInsight.VehicleCount

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				vehicleCount.LandAliveLocal, _ = strconv.Atoi(argValue)
			case 1:
				vehicleCount.LandAliveRemote, _ = strconv.Atoi(argValue)
			case 2:
				vehicleCount.LandDeadLocal, _ = strconv.Atoi(argValue)
			case 3:
				vehicleCount.LandDeadRemote, _ = strconv.Atoi(argValue)
			case 4:
				vehicleCount.AirAliveLocal, _ = strconv.Atoi(argValue)
			case 5:
				vehicleCount.AirAliveRemote, _ = strconv.Atoi(argValue)
			case 6:
				vehicleCount.AirDeadLocal, _ = strconv.Atoi(argValue)
			case 7:
				vehicleCount.AirDeadRemote, _ = strconv.Atoi(argValue)
			case 8:
				vehicleCount.ShipAliveLocal, _ = strconv.Atoi(argValue)
			case 9:
				vehicleCount.ShipAliveRemote, _ = strconv.Atoi(argValue)
			case 10:
				vehicleCount.ShipDeadLocal, _ = strconv.Atoi(argValue)
			case 11:
				vehicleCount.ShipDeadRemote, _ = strconv.Atoi(argValue)
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.LogVehicleCount(vehicleCount)
	case "unit_count":
		var unitCount armaInsight.UnitCount

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				unitCount.AliveLocal, _ = strconv.Atoi(argValue)
			case 1:
				unitCount.AliveRemote, _ = strconv.Atoi(argValue)
			case 2:
				unitCount.DeadLocal, _ = strconv.Atoi(argValue)
			case 3:
				unitCount.DeadRemote, _ = strconv.Atoi(argValue)
			case 4:
				unitCount.SoldierWB, _ = strconv.Atoi(argValue)
			case 5:
				unitCount.SoldierEB, _ = strconv.Atoi(argValue)
			case 6:
				unitCount.SoldierGB, _ = strconv.Atoi(argValue)
			case 7:
				unitCount.Civilian, _ = strconv.Atoi(argValue)
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.LogUnitCount(unitCount)
	case "player_count":
		var playerCount armaInsight.PlayerCount

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				playerCount.Total, _ = strconv.Atoi(argValue)
			case 1:
				playerCount.HeadlessClient, _ = strconv.Atoi(argValue)
			case 2:
				playerCount.East, _ = strconv.Atoi(argValue)
			case 3:
				playerCount.West, _ = strconv.Atoi(argValue)
			case 4:
				playerCount.Resistance, _ = strconv.Atoi(argValue)
			case 5:
				playerCount.Civilian, _ = strconv.Atoi(argValue)
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.LogPlayerCount(playerCount)
	case "performance":
		var performance armaInsight.Performance

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				performance.TickTime, _ = strconv.ParseFloat(argValue, 64)
			case 1:
				performance.FPS, _ = strconv.ParseFloat(argValue, 64)
			case 2:
				performance.FPSMin, _ = strconv.ParseFloat(argValue, 64)
			case 3:
				performance.ActiveScriptSpawn, _ = strconv.Atoi(argValue)
			case 4:
				performance.ActiveScriptExecVM, _ = strconv.Atoi(argValue)
			case 5:
				performance.ActiveScriptExec, _ = strconv.Atoi(argValue)
			case 6:
				performance.ActiveScriptExecFSM, _ = strconv.Atoi(argValue)
			case 7:
				performance.CBAWaitAndExec, _ = strconv.Atoi(argValue)
			case 8:
				performance.CBAWaitUntilAndExec, _ = strconv.Atoi(argValue)
			case 9:
				performance.CBAFrameHandler, _ = strconv.Atoi(argValue)
			case 10:
				performance.CBAFrameHandlerHandles, _ = strconv.Atoi(argValue)
			case 11:
				performance.CLibFrameHandler, _ = strconv.Atoi(argValue)
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.LogPerformance(performance)
	case "simulation":
		var simulation armaInsight.Simulation

		var offset = unsafe.Sizeof(uintptr(0))
		for index := C.int(0); index < argc; index++ {
			argValue := C.GoString(*argv)

			switch index {
			case 0:
				simulation.SimulationTotal, _ = strconv.Atoi(argValue)
			case 1:
				simulation.SimulationEnabled, _ = strconv.Atoi(argValue)
			case 2:
				simulation.DynamicSimulationSystemEnabled, _ = strconv.Atoi(argValue)
			case 3:
				simulation.DynamicSimulationEnabled, _ = strconv.Atoi(argValue)
			}

			argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
		}

		go insight.LogSimulation(simulation)
	}
}

//export goRVExtension
func goRVExtension(output *C.char, outputsize C.size_t, input *C.char) {
	checkInit()
	function := C.GoString(input)
	go insight.CallIncrease(function)
}

//export goRVExtensionRegisterCallback
func goRVExtensionRegisterCallback(fnc C.extensionCallback) {
	extensionCallbackFnc = fnc
}

func checkInit() {
	insightLock.Lock()
	defer insightLock.Unlock()

	if insight != nil {
		return
	}

	var err error
	insight, err = armaInsight.NewInsight()
	if err != nil {
		panic(err.Error())
	}

	go func() {
		for v := range insight.Callback {
			returnCode := runExtensionCallback(v.Function, v.Data)
			logrus.Traceln("callback", v.Function, returnCode)
		}
	}()
}

func main() {}
