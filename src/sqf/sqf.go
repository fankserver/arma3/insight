package sqf

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"reflect"
	"strconv"
)

func UnquotedUnmarshal(data []byte, obj interface{}) error {
	data = bytes.ReplaceAll(data, []byte(`""`), []byte(`"`))
	data = bytes.TrimPrefix(data, []byte(`"`))
	data = bytes.TrimSuffix(data, []byte(`"`))
	return Unmarshal(data, obj)
}

func Unmarshal(data []byte, obj interface{}) error {
	rv := reflect.ValueOf(obj)

	decoder := json.NewDecoder(bytes.NewReader(data))

	var err error
	switch rv.Elem().Kind() {
	case reflect.Slice:
		err = decodeArray(decoder, rv.Elem(), false)
	case reflect.Struct:
		err = decodeStruct(decoder, rv.Elem(), false)
	}
	return err
}

func decodeArray(decoder *json.Decoder, rv reflect.Value, started bool) error {
	elem := rv.Type().Elem()
	arrayStartFound := started
	for {
		tok, err := decoder.Token()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		if tok == json.Delim(91) { // array open
			if !arrayStartFound {
				arrayStartFound = true
				continue
			} else {
				switch elem.Kind() {
				case reflect.Slice:
					err = decodeArray(decoder, rv.Elem(), true)
				case reflect.Struct:
					r := reflect.New(rv.Type().Elem())
					err = decodeStruct(decoder, r.Elem(), true)
					rv.Set(reflect.Append(rv, r.Elem()))
				default:
					err = fmt.Errorf("unknown start")
				}
				if err != nil {
					return err
				}
				continue
			}
		} else if tok == json.Delim(93) { // array close
			return nil
		}

		if !arrayStartFound {
			return fmt.Errorf("start not found")
		}

		switch typ := tok.(type) {
		case string:
			if elem.Kind() != reflect.String {
				return fmt.Errorf("expected string got %s", elem.Kind())
			}
			rv.Set(reflect.Append(rv, reflect.ValueOf(typ)))
		case float64:
			switch elem.Kind() {
			case reflect.Float64:
				rv.Set(reflect.Append(rv, reflect.ValueOf(typ)))
			case reflect.Int:
				rv.Set(reflect.Append(rv, reflect.ValueOf(int(typ))))
			default:
				return fmt.Errorf("unknown type: %s", rv.Type().Kind())
			}
		case bool:
			if elem.Kind() != reflect.Bool {
				return fmt.Errorf("expected bool got %s", elem.Kind())
			}
			rv.Set(reflect.Append(rv, reflect.ValueOf(typ)))
		default:
			return fmt.Errorf("unknown array type %s", reflect.TypeOf(tok).Kind())
		}
	}

	return nil
}

func decodeStruct(decoder *json.Decoder, rv reflect.Value, started bool) error {
	structType := reflect.TypeOf(rv.Interface())
	index := -1
	if started {
		index = 0
	}

	for {
		tok, err := decoder.Token()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		if tok == json.Delim(91) { // array open
			if index < 0 {
				index = 0
				continue
			} else {
				fieldID := -1
				for i := 0; i < structType.NumField(); i++ {
					field := structType.Field(i)
					sqf := field.Tag.Get("sqf")
					if sqf == "" {
						continue
					}
					if sqf != strconv.Itoa(index) {
						continue
					}
					fieldID = i
					break
				}
				if fieldID < 0 {
					return fmt.Errorf("unknown field for array")
				}

				switch rv.Field(fieldID).Kind() {
				case reflect.Slice:
					err = decodeArray(decoder, rv.Field(fieldID), true)
				case reflect.Struct:
					err = decodeStruct(decoder, rv.Field(fieldID), true)
				default:
					err = fmt.Errorf("unknown start")
				}
				if err != nil {
					return err
				}
				index++
				continue
			}
		} else if tok == json.Delim(93) { // array close
			return nil
		}

		if index < 0 {
			return fmt.Errorf("start not found")
		}

		fieldID := -1
		for i := 0; i < structType.NumField(); i++ {
			field := structType.Field(i)
			sqf := field.Tag.Get("sqf")
			if sqf == "" {
				continue
			}
			if sqf != strconv.Itoa(index) {
				continue
			}
			fieldID = i
			break
		}
		if fieldID < 0 {
			continue
		}

		switch typ := tok.(type) {
		case string:
			if structType.Field(fieldID).Type.Kind() != reflect.String {
				return fmt.Errorf("expected string got %s", structType.Field(fieldID).Type.Kind())
			}
			rv.Field(fieldID).SetString(typ)
		case float64:
			switch structType.Field(fieldID).Type.Kind() {
			case reflect.Float64:
				rv.Field(fieldID).SetFloat(typ)
			case reflect.Int:
				rv.Field(fieldID).SetInt(int64(typ))
			default:
				return fmt.Errorf("unable to use kind %s for float64", structType.Field(fieldID).Type.Kind())
			}
		case bool:
			if structType.Field(fieldID).Type.Kind() != reflect.Bool {
				return fmt.Errorf("expected bool got %s", structType.Field(fieldID).Type.Kind())
			}
			rv.Field(fieldID).SetBool(typ)
		default:
			return fmt.Errorf("unknown struct type %s", reflect.TypeOf(tok).Kind())
		}

		index++
	}

	return nil
}
