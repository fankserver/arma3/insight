package sqf_test

import (
	"testing"

	"gitlab.com/fankserver/arma3/insight/src/sqf"
)

func TestUnmarshal(t *testing.T) {
	//t.Run("asd", func(t *testing.T) {
	//	sqf.Unmarshal([]byte(`["asd",1]`), nil)
	//})
	//t.Run("asd2", func(t *testing.T) {
	//	sqf.Unmarshal([]byte(`["asd",[1,2,3], "dddd"]`), nil)
	//})
	t.Run("slice", func(t *testing.T) {
		t.Run("string", func(t *testing.T) {
			var val []string
			err := sqf.Unmarshal([]byte(`["asd","dddd"]`), &val)
			if err != nil {
				t.Error(err.Error())
				return
			}
			if len(val) != 2 {
				t.Error("wrong array length", len(val))
				return
			}
			if val[0] != "asd" {
				t.Error("wrong array value0", val[0])
			}
			if val[1] != "dddd" {
				t.Error("wrong array value1", val[1])
			}
		})
		t.Run("float64", func(t *testing.T) {
			var val []float64
			err := sqf.Unmarshal([]byte(`[1,2.3,4.56]`), &val)
			if err != nil {
				t.Error(err.Error())
				return
			}
			if len(val) != 3 {
				t.Error("wrong array length", len(val))
				return
			}
			if val[0] != 1 {
				t.Error("wrong array value0", val[0])
			}
			if val[1] != 2.3 {
				t.Error("wrong array value1", val[1])
			}
			if val[2] != 4.56 {
				t.Error("wrong array value2", val[2])
			}
		})
		t.Run("int", func(t *testing.T) {
			var val []int
			err := sqf.Unmarshal([]byte(`[1,2,4]`), &val)
			if err != nil {
				t.Error(err.Error())
				return
			}
			if len(val) != 3 {
				t.Error("wrong array length", len(val))
				return
			}
			if val[0] != 1 {
				t.Error("wrong array value0", val[0])
			}
			if val[1] != 2 {
				t.Error("wrong array value1", val[1])
			}
			if val[2] != 4 {
				t.Error("wrong array value2", val[2])
			}
		})
		//t.Run("slice", func(t *testing.T) {
		//	var val [][]int
		//	err := sqf.Unmarshal([]byte(`[[1],[2,4]]`), &val)
		//	if err != nil {
		//		t.Error(err.Error())
		//		return
		//	}
		//	if len(val) != 2 {
		//		t.Error("wrong array length", len(val))
		//		return
		//	}
		//	if len(val[0]) != 1 {
		//		t.Error("wrong array0 length", len(val[0]))
		//		return
		//	}
		//	if len(val[1]) != 2 {
		//		t.Error("wrong array0 length", len(val[1]))
		//		return
		//	}
		//	if val[0][0] != 1 {
		//		t.Error("wrong array value0", val[0])
		//	}
		//	if val[1][0] != 2 {
		//		t.Error("wrong array value1", val[1])
		//	}
		//	if val[1][1] != 4 {
		//		t.Error("wrong array value2", val[2])
		//	}
		//})
	})
	t.Run("struct", func(t *testing.T) {
		t.Run("basic", func(t *testing.T) {
			var val struct {
				A int     `sqf:"0"`
				B float64 `sqf:"2"`
				C string  `sqf:"1"`
			}
			err := sqf.Unmarshal([]byte(`[1,"2",4.23]`), &val)
			if err != nil {
				t.Error(err.Error())
				return
			}
			if val.A != 1 {
				t.Error("wrong array valueA", val.A)
			}
			if val.B != 4.23 {
				t.Error("wrong array valueB", val.B)
			}
			if val.C != "2" {
				t.Error("wrong array valueC", val.C)
			}
		})
		t.Run("slice", func(t *testing.T) {
			var val struct {
				A []int     `sqf:"0"`
				B []float64 `sqf:"2"`
				C []string  `sqf:"1"`
			}
			err := sqf.Unmarshal([]byte(`[[1],["2"],[4.23]]`), &val)
			if err != nil {
				t.Error(err.Error())
				return
			}
			if len(val.A) != 1 {
				t.Error("wrong arrayA length", len(val.A))
				return
			}
			if len(val.B) != 1 {
				t.Error("wrong arrayB length", len(val.B))
				return
			}
			if len(val.C) != 1 {
				t.Error("wrong arrayC length", len(val.C))
				return
			}
			if val.A[0] != 1 {
				t.Error("wrong array valueA", val.A[0])
			}
			if val.B[0] != 4.23 {
				t.Error("wrong array valueB", val.B[0])
			}
			if val.C[0] != "2" {
				t.Error("wrong array valueC", val.C[0])
			}
		})
		t.Run("struct", func(t *testing.T) {
			var val struct {
				A struct {
					A int `sqf:"0"`
				} `sqf:"0"`
				B struct {
					A float64 `sqf:"0"`
				} `sqf:"2"`
				C struct {
					A string `sqf:"0"`
				} `sqf:"1"`
			}
			err := sqf.Unmarshal([]byte(`[[1],["2"],[4.23]]`), &val)
			if err != nil {
				t.Error(err.Error())
				return
			}
			if val.A.A != 1 {
				t.Error("wrong array valueA", val.A.A)
			}
			if val.B.A != 4.23 {
				t.Error("wrong array valueB", val.B.A)
			}
			if val.C.A != "2" {
				t.Error("wrong array valueC", val.C.A)
			}
		})
	})
}

type PositionVehicle struct {
	Name      string   `sqf:"0"`
	ID        string   `sqf:"1"`
	Position  Position `sqf:"2"`
	Direction float64  `sqf:"3"`
	Class     string   `sqf:"4"`
	Icon      string   `sqf:"5"`
	Faction   int      `sqf:"6"`
	Group     string   `sqf:"7"`
	Crew      []string `sqf:"8"`
	Cargo     []string `sqf:"9"`
	IconName  string   `sqf:"10"`
}
type Position struct {
	X float64 `sqf:"0"`
	Y float64 `sqf:"1"`
	Z float64 `sqf:"2"`
}

func TestUnquotedUnmarshal(t *testing.T) {
	t.Run("test1", func(t *testing.T) {
		var obj []PositionVehicle
		err := sqf.UnquotedUnmarshal([]byte(`"[[""O Alpha 2-6:1 (Fank) REMOTE"",""76561198000074241"",[8597.67,2250.95,0.0690131],333,""OPT_CUP_O_Ural_TKA"",""iconTruck"",0,""Alpha 2-6"",[""76561198000074241""],[],""icomap_ural_ca""]]"`), &obj)
		if err != nil {
			t.Error(err.Error())
			return
		}
		if len(obj) != 1 {
			t.Error("wrong array length")
		}
		if obj[0].Name != "O Alpha 2-6:1 (Fank) REMOTE" {
			t.Error("wrong name")
		}
	})
}
